<?php
session_start();
$username = "";
$email = "";
$errors = array();
$_SESSION['success'] = "";
// connect to database
$db = mysqli_connect('localhost', 'root', '', 'registration');
if(isset($_POST['register_user']))
{
    // receive all input values from the form
    $full_name = mysqli_real_escape_string($db, $_POST['full_name']);
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $experience = mysqli_real_escape_string($db, $_POST['experience']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $contact_number = mysqli_real_escape_string($db, $_POST['contact_no']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
    // form validation: ensure that the form is correctly filled
    if(empty($username))
    {
	array_push($errors, "Username is required");
    }
    if(empty($email))
    {
	array_push($errors, "Email is required");
    }
    if(empty($password_1))
    {
	array_push($errors, "Password is required");
    }
    if($password_1 != $password_2)
    {
	array_push($errors, "The two passwords do not match");
    }
    // register user if there are no errors in the form
    if(count($errors) == 0)
    {
	$password = $password_1;
	$query = "INSERT INTO trainers (name,username, email,contact_number, password,experience) 
					  VALUES('$full_name','$username', '$email','$contact_number', '$password','$experience')";
	$result=mysqli_query($db, $query);

	$_SESSION['timeout'] = time();
	$_SESSION['username'] = $username;
//	$_SESSION['student_id'] = $student[0];
	$_SESSION['success'] = "You are now logged in";
//	$_SESSION['username'] = $username;
//	$_SESSION['success'] = "You are now logged in";
	header('location: index.php');
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Trainer | Log in</title>
    <?php include '../admin/includes/styles.php' ?>
</head>
<body class="hold-transition login-page" style="background-image: url('../admin/dist/img/bg.png')">
<div class="login-box">
    <div class="login-logo">
        <a href="login.php" class="text-white"><b>Car & Scooty </b> Driving School</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign up to start your session</p>

            <form method="post" action="register.php">

		<?php include('errors.php'); ?>

                <div class="input-group">
                    <input placeholder="Full Name" type="text" name="full_name" class="form-control">
                </div>
                <div class="input-group">
                    <input type="text" placeholder="Username" name="username" class="form-control">
                </div>
                <div class="input-group">
                    <input type="text" placeholder="Experience" name="experience" class="form-control">
                </div>
                <div class="input-group">
                    <input placeholder="Email" type="email" name="email" class="form-control">
                </div>
                <div class="input-group">
                    <input placeholder="Contact Number" type="text" name="contact_no" class="form-control">
                </div>

                <div class="input-group">
                    <input placeholder="Password" type="password" name="password_1" class="form-control">
                </div>
                <div class="input-group">
                    <input placeholder="Confirm Password" type="password" class="form-control" name="password_2">
                </div>
                <div class="col-4">
                    <button type="submit" name="register_user" class="btn btn-primary btn-block">Register</button>
                </div>

                <p>
                    Already a member? <a href="login.php">Sign in</a>
                </p>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>

<?php include '../admin/includes/scripts.php' ?>
</body>
</html>
