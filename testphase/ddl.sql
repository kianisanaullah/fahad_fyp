CREATE SCHEMA if not exists registrations;

CREATE TABLE `students`
(
    `student_id`       int(12)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `full_name` varchar(25) NOT NULL,
    `username` varchar(25) NOT NULL,
    `contact_no` integer(15) NOT NULL,
    `email`    varchar(25) NOT NULL,
    `password` varchar(25) NOT NULL,
    `address` varchar(25) NOT NULL


) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
##################################################################################################

CREATE TABLE `trainers`
(
    `trainer_id`       int(12)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(25) NOT NULL,
    `email`    varchar(25) NOT NULL,
    `experience` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
##################################################################################################

CREATE TABLE `trainers_students`
(
    `id`       int(12)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `trainer_id`       int(12)     ,
    `student_id`       int(12)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
