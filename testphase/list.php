<?php include('server.php') ;

$query = "SELECT * FROM students WHERE student_type=2 ";
	$results = mysqli_query($db, $query);

	?>
<!DOCTYPE html>
<html>
<head>
	<title>Update Student</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 15px;
        text-align: left;
    }
    .center {
        margin-left: auto;
        margin-right: auto;
    }

</style>
<body style="background: green">
	<div class="header">
		<h2>Students List</h2>
        <a href="index.php">Add New</a>
	</div>
    <br>
    <br>
	<table class="center" >
        <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
        </thead>
        <tbody>

            <?php


                while( $row=$results->fetch_row())
                {
                    ?>
                <tr>
                    <td><?php echo  $row[0] ?></td>
                    <td><?php echo $row[1] ?></td>
                    <td><?php echo $row[4] ?></td>
                    <td>
                        <a href="edit.php?id=<?php echo  $row[0] ?>" style="background: blue; color: white">Edit</a>
                        <a href="list.php?delete_id=<?php echo  $row[0] ?>" style="background: red">Delete</a>
                    </td>
                </tr>
            <?php
                }
            ?>


        </tbody>

    </table>
</body>
</html>
