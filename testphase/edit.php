<?php include('server.php') ;

$id=$_GET['id'];
$query = "SELECT * FROM students WHERE student_id='$id'";
	$results = mysqli_query($db, $query);
	$result=$results->fetch_row();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Update Student</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background: skyblue">
	<div class="header">
		<h2>Update Student Form</h2>
	</div>

	<form method="post" action="edit.php">
        <input type="hidden" name="id"  value="<?php echo $result[0]; ?>">
		<?php include('errors.php'); ?>

		<div class="input-group">
			<label>Full Name</label>
			<input type="text" name="full_name" placeholder="Name" value="<?php echo $result[1]; ?>">
		</div>
        <div class="input-group">
			<label>Username</label>
			<input type="text" name="username" placeholder="Username" value="<?php echo $result[2]; ?>">
		</div>
		<div class="input-group">
			<label>Email</label>
			<input type="email" name="email" placeholder="Email" value="<?php echo $result[4]; ?>">
		</div>
        <div class="input-group">
			<label>Contact Number</label>
			<input type="text" name="contact_no" placeholder="Contact Number" value="<?php echo $result[3]; ?>">
		</div>
        <div class="input-group">
            <label>Address</label>
            <input type="text" name="address" placeholder="Address" value="<?php echo $result[6]; ?>">
        </div>
		<div class="input-group">
			<button type="submit" class="btn" name="update_user">Update</button>
		</div>
		<p>
			Go to student list <a href="list.php">List</a>
		</p>
	</form>
</body>
</html>ss
