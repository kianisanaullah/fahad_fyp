<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background: skyblue">
	<div class="header">
		<h2>Register</h2>
		
	</div>

	<form method="post" action="index.php">

		<?php include('errors.php'); ?>
        <div class="input-group">
            <label>Type</label>
            <select name="type" id="">
                <option value="" selected>Select Type</option>
                <option value="1">Student</option>
                <option value="2">Trainer</option>
            </select>
        </div>
		<div class="input-group">
			<label>Full Name</label>
			<input type="text" name="full_name" placeholder="Name">
		</div>
        <div class="input-group">
			<label>Username</label>
			<input type="text" name="username" placeholder="Username" value="<?php echo $username; ?>">
		</div>
		<div class="input-group">
			<label>Email</label>
			<input type="email" name="email" placeholder="Email" value="<?php echo $email; ?>">
		</div>
        <div class="input-group">
			<label>Contact Number</label>
			<input type="text" name="contact_no" placeholder="Contact Number">
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password_1" placeholder="Password">
		</div>
		<div class="input-group">
			<label>Confirm password</label>
			<input type="password" name="password_2" placeholder="Confirm Password">
		</div>
        <div class="input-group">
            <label>Address</label>
            <input type="text" name="address" placeholder="Address">
        </div>
		<div class="input-group">
			<button type="submit" class="btn" name="reg_user">Register</button>
		</div>
		<p>
			Already a member? <a href="login.php">Sign in</a>
		</p>
	</form>
</body>
</html>
