
<?php include('server.php') ;
$students_count_query = "SELECT count(*) FROM registration.students";
$students_count_result = mysqli_query($db, $students_count_query);
$student_count_row = $students_count_result->fetch_row();
$students_count=$student_count_row[0];

$trainers_count_query = "SELECT count(*) FROM registration.trainers";
$trainers_count_result = mysqli_query($db, $trainers_count_query);
$trainer_count_row = $trainers_count_result->fetch_row();
$trainers_count=$trainer_count_row[0];

$payment_count_query = "SELECT count(*) FROM registration.payments";
$payment_count_result = mysqli_query($db, $payment_count_query);
$payment_count_row = $payment_count_result->fetch_row();
$payment_count=$payment_count_row[0];
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fahad FYP</title>
<?php include 'includes/styles.php'?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php
    include 'includes/nav_top.php';
    include 'includes/side_bar.php';
  ?>


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $students_count?></h3>

                <p>Students Info</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
<!--              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?php echo $trainers_count?><sup style="font-size: 20px"></sup></h3>

                <p>Trainers Info</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
<!--              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo $payment_count?></h3>

                <p>Payments Info</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
<!--              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>0</h3>

                <p>Vehicle info</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
<!--              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">



            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">



          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include 'includes/footer.php'?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php
    include 'includes/scripts.php';
?>
</body>
</html>
