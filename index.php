<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Car & Scooty Driving School</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="admin/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="admin/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="../../index2.html"><b>Car & Scooty Driving School</b></a>
    </div>
    <!-- User name -->
    <div class="lockscreen-name">Select Type of user</div>
    <!--    <div class="row">-->
    <!--        <div class="col-md-4">-->
    <!--            <p>Admin</p>-->
    <!--        </div>-->
    <!--        <div class="col-md-4">-->
    <!--            <p>Student</p>-->
    <!--        </div>-->
    <!--        <div class="col-md-4">-->
    <!--            <p>Trainer</p>-->
    <!--        </div>-->
    <!--    </div>-->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-4 col-6">
                    <a href="../FYP/admin/login.php">
                        <!-- small box -->
                        <div  class="small-box bg-info">
                            <div class="inner">
                                <p>Admin </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-6">
                    <a href="../FYP/trainer/login.php">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <p>Trainer</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-user"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-6">
                    <a href="../FYP/student/login.php">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <p>Student</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-users"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

</div>
<!-- /.lockscreen-item -->
</div>
<!-- /.center -->

<!-- jQuery -->
<script src="admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
