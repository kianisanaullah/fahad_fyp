
<?php include('server.php') ;
$student_id=$_SESSION['student_id'];

$trainers_query = "SELECT COUNT('*') FROM trainers_students WHERE student_id='$student_id'";
$trainers_count_result = mysqli_query($db, $trainers_query);
$trainer_count_row = $trainers_count_result->fetch_row();
$trainers_count=$trainer_count_row[0];

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fahad FYP</title>
<?php include 'includes/styles.php'?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php
    include 'includes/nav_top.php';
    include 'includes/side_bar.php';
  ?>


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 offset-2 connectedSortable">

              <video width="700" height="400" controls>
                  <source src="../tutorials/1.mp4" type="video/mp4">
<!--                  <source src="movie.ogg" type="video/ogg">-->
                  Your browser does not support the video tag.
              </video>
              <br>
              <br>
              <br>
            <!-- /.card -->
          </section>
          <section class="col-lg-7 offset-2 connectedSortable">

              <video width="700" height="400" controls>
                  <source src="../tutorials/2.mp4" type="video/mp4">
<!--                  <source src="movie.ogg" type="video/ogg">-->
                  Your browser does not support the video tag.
              </video>

            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">



          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include 'includes/footer.php'?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php
    include 'includes/scripts.php';
?>
</body>
</html>
