<?php
session_start();
$username = "";
$email = "";
$errors = array();
$_SESSION['success'] = "";
// connect to database
$db = mysqli_connect('localhost', 'root', '', 'registration');

if(isset($_POST['login_user']))
{
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $password = mysqli_real_escape_string($db, $_POST['password']);
    if(empty($username))
    {
	array_push($errors, "Username is required");
    }
    if(empty($password))
    {
	array_push($errors, "Password is required");
    }
    if(count($errors) == 0)
    {







	$query = "SELECT * FROM students WHERE username='$username' AND password='$password'";
	$results = mysqli_query($db, $query);

	$student_result = mysqli_query($db, $query);
	$student_row = $student_result->fetch_row();
	$student=$student_row;


	$results = mysqli_query($db, $query);
	if(mysqli_num_rows($results) == 1)
	{
	    $_SESSION['timeout'] = time();
	    $_SESSION['username'] = $username;
	    $_SESSION['student_id'] = $student[0];
	    $_SESSION['success'] = "You are now logged in";
	    $results = mysqli_query($db, $query);
//	    $row = $results->fetch_row();
//	    {
//	        if($row[7]==3)
//            {
	    header('location: index.php');
//	        }
//	    }
	} else
	{
	    array_push($errors, "Wrong username/password combination");
	}
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Student | Log in</title>
    <?php include '../admin/includes/styles.php' ?>
</head>
<body class="hold-transition login-page" style="background-image: url('../admin/dist/img/bg.png')">
<div class="login-box">
    <div class="login-logo">
        <a href="login.php" class="text-white"><b>Car & Scooty </b> Driving School</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="login.php" method="post">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" name="login_user" class="btn btn-primary btn-block">Sign In</button>
                    </div>

                    <div class="col-4">
                        <a href="register.php" class="btn btn-success" type="button">Register</a>
                    </div>
                    <!-- /.col -->
                </div>
            </form>


            <!-- /.social-auth-links -->

<!--            	    <p class="mb-1">-->
<!--            		<a href="forgot-password.html">I forgot my password</a>-->
<!--            	    </p>-->
        </div>
        <!-- /.login-card-body -->
    </div>
</div>

<?php include '../admin/includes/scripts.php' ?>
</body>
</html>
