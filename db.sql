create table if not exists payments
(
	id int not null,
	student_id int not null,
	bill_number int not null,
	payment_date varchar(100) not null,
	amount int not null
)
charset=latin1;

create table if not exists students
(
	student_id int auto_increment
		primary key,
	full_name varchar(25) not null,
	username varchar(25) not null,
	contact_no int not null,
	email varchar(25) not null,
	password varchar(25) not null,
	address varchar(25) not null
)
charset=latin1;

create table if not exists trainers
(
	trainer_id int auto_increment
		primary key,
	name varchar(25) not null,
	email varchar(25) not null,
	experience varchar(50) not null,
	contact_number varchar(12) null,
	password varchar(15) null,
	username varchar(200) null
)
charset=latin1;

create table if not exists trainers_students
(
	id int auto_increment
		primary key,
	trainer_id int null,
	student_id int null
)
charset=latin1;

create table if not exists vehicles
(
	id int auto_increment
		primary key,
	vehicle_type varchar(100) null,
	vehicle_avaialbility int null
);

create table if not exists vehicles_students
(
	id int auto_increment
		primary key,
	vehicle_id int null,
	student_id int null
);

